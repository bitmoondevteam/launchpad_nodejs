import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RedisService } from './handler/redis.service';
import { ProjectModule } from './project/project.module';
import { StakingModule } from './staking/staking.module';
import { TaskService } from './task.service';

@Module({
  imports: [ScheduleModule.forRoot(), StakingModule, ProjectModule],
  controllers: [AppController],
  providers: [AppService, TaskService, RedisService],
})
export class AppModule {}
