import { Injectable } from '@nestjs/common';
import { Sequelize } from 'sequelize';
import Redis from 'ioredis';

@Injectable()
export class AppService {
  //Database connection
  private static sequelizeConnection: Sequelize;
  public static getSequelizeConnection = () => {
    if (!AppService.sequelizeConnection) {
      const username = process.env.DB_USER || 'launchpad_uat';
      const password = process.env.DB_PASSWORD || 'YzQ2ZWZhZDZjNGNkM2M4MGFhZ';
      const host = process.env.DB_HOST || 'mysql.uat.bitmoon.review';
      const defaultSchema = process.env.DB_SCHEMA || 'launchpad_uat';

      const connectionString = `mysql://${username}:${password}@${host}/${defaultSchema}`;
      AppService.sequelizeConnection = new Sequelize(connectionString);
    }
    return AppService.sequelizeConnection;
  };

  //Redis connection
  private static redisClient: Redis.Redis;
  public static getRedisClient = async () => {
    if (!AppService.redisClient) {
      const host = process.env.REDIS_HOST || 'redis-uat.bitmoon.review';
      const port = process.env.REDIS_PORT || 6385;
      AppService.redisClient = new Redis(`redis://${host}:${port}`);
    }

    return AppService.redisClient;
  };

  public static tokenPrice: { [key: string]: { [key: string]: number } } = {};
  public static stakingAPR: { [key: string]: { [key: string]: number } } = {};
}
