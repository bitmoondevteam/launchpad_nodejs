import { Injectable } from '@nestjs/common';
import { AppService } from 'src/app.service';
import { DEFAULT_CACHE_TIMEOUT } from 'src/config/config';

@Injectable()
export class RedisService {
  async retrieveData(
    key: string,
    process: () => Promise<string>,
  ): Promise<string> {
    const redisClient = await AppService.getRedisClient();
    let data = await redisClient.get(key);
    if (!data) {
      data = await process();
      if (data) {
        await redisClient.set(key, data, 'EX', DEFAULT_CACHE_TIMEOUT);
      }
    }
    return data;
  }
}
