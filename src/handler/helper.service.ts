import { Injectable } from '@nestjs/common';

@Injectable()
export class HelperService {
  formatString(source: string, ...args: any) {
    return source.replace(/{(\d+)}/g, (match, number) => {
      if (typeof args[number] !== 'undefined') {
        return args[number];
      }
      throw new Error(`Missing parameter for index {${number}}`);
    });
  }
}
