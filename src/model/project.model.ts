export type Project = {
  id: string;
  name: string;
  description: string;
  project_type: number;
  token_address: string;
  token_symbol: string;
  minimum_allocation: string;
  maximum_allocation: string;
  token_on_sale: string;
  sale_time: Date;
  end_time: Date;
  process_time: Date;
  master_address: string;
  month_lock: number;
  preclaim_ratio: number;
  fund_token_address: string;
  fund_token_ratio: string;
  fund_token_symbol: string;
};
