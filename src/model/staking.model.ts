export type StakingItem = {
  id: number;
  smart_chef_contract: string;
  start_block: string;
  end_block: string;
  pool_limit_per_user: string;
  stake_token: string;
  stake_token_symbol: string;
  reward_token: string;
  reward_token_symbol: string;
  token: string;
  quote_token: string;
  with_ido: boolean;
  is_lp_staking: boolean;
  chain_id: number;
  apr: number;
  allocations: [string];
};
