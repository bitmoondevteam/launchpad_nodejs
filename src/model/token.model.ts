export type TokenReserve = {
  id: number;
  contract_address: string;
  token_decimal: number;
  token_symbol: string;
  route: TokenReserveRoute[];
};

export type TokenReserveRoute = {
  source_token: string;
  source_token_decimal: number;
  target_token: string;
  target_token_decimal: number;
};
