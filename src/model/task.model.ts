import { StakingItem } from './staking.model';
import { TokenReserve } from './token.model';

export type TaskNetworkInfo = {
  chain_id: number;
  network_rpc: string;
  pair_code_hash: string;
  factory_contract: string;
  token_info: TokenReserve[];
};

export type TaskStakingAPR = {
  chain_id: number;
  network_rpc: string;
  block_time: number;
  staking_info: StakingItem[];
};
