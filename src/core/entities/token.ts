import { Token } from '@uniswap/sdk-core';

export class CustomToken extends Token {
  readonly isStableCoin: boolean;
  constructor(
    chainId: number,
    address: string,
    decimals: number,
    isStableCoin: boolean,
    symbol?: string,
    name?: string,
  ) {
    super(chainId, address, decimals, symbol, name);
    this.isStableCoin = isStableCoin;
  }
}
