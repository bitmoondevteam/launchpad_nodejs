export const getWeiUnit = (decimal: number) => {
  switch (decimal) {
    case 1:
      return 'wei';
    case 3:
      return 'kwei';
    case 6:
      return 'mwei';
    case 9:
      return 'gwei';
    case 12:
      return 'micro';
    case 15:
      return 'milli';
    case 18:
      return 'ether';
    case 21:
      return 'kether';
    case 24:
      return 'mether';
    case 27:
      return 'gether';
    case 30:
      return 'tether';
  }
};
