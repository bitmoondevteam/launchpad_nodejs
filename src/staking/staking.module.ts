import { Module } from '@nestjs/common';
import { StakingService } from './staking.service';
import { StakingController } from './staking.controller';
import { RedisService } from 'src/handler/redis.service';

@Module({
  imports: [],
  controllers: [StakingController],
  providers: [StakingService, RedisService],
})
export class StakingModule {}
