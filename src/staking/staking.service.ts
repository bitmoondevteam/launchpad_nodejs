import { Injectable } from '@nestjs/common';
import { QueryTypes } from 'sequelize/dist';
import { AppService } from 'src/app.service';
import { StakingItem } from 'src/model/staking.model';
import CACHE from 'src/config/cache';
import { RedisService } from 'src/handler/redis.service';

@Injectable()
export class StakingService {
  constructor(private readonly redisService: RedisService) {}

  async getListStaking(): Promise<StakingItem[]> {
    const dataString = await this.redisService.retrieveData(
      CACHE.STAKING_LIST,
      async () => {
        const stakingItems =
          await AppService.getSequelizeConnection().query<StakingItem>(
            'SELECT * FROM staking_item',
            { type: QueryTypes.SELECT },
          );
        return JSON.stringify(stakingItems);
      },
    );
    const result: StakingItem[] = JSON.parse(dataString);
    result.forEach((item) => {
      if (AppService.stakingAPR[item.chain_id]) {
        item.apr =
          AppService.stakingAPR[item.chain_id][item.smart_chef_contract] || 0;
      }
    });

    return result;
  }

  async getListIdoStaking(): Promise<StakingItem[]> {
    const dataString = await this.redisService.retrieveData(
      CACHE.STAKING_IDO_LIST,
      async () => {
        const stakingItems =
          await AppService.getSequelizeConnection().query<StakingItem>(
            `
              SELECT si.*, (
                SELECT JSON_ARRAYAGG(sia.allocation)
                FROM staking_item_allocation sia
                WHERE sia.staking_id = si.id
              ) as allocations
              FROM staking_item si
              WHERE si.with_ido = 1
            `,
            { type: QueryTypes.SELECT },
          );
        return JSON.stringify(stakingItems);
      },
    );
    return JSON.parse(dataString);
  }
}
