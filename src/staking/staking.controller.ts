import { Controller, Get } from '@nestjs/common';
import { StakingService } from './staking.service';

@Controller('staking')
export class StakingController {
  constructor(private readonly stakingService: StakingService) {}

  @Get('/list')
  async getListStaking() {
    return this.stakingService.getListStaking();
  }

  @Get('/ido')
  async getListIdoStaking() {
    return this.stakingService.getListIdoStaking();
  }
}
