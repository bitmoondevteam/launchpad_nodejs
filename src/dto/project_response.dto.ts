import { Project } from 'src/model/project.model';

export type ProjectResponseDTO = {
  data: Project;
  metadata: {
    page: number;
    limit: number;
    total: number;
  };
};
