import { Cron } from '@nestjs/schedule';
import { Injectable } from '@nestjs/common';
import { RedisService } from './handler/redis.service';
import { Pair } from './core/entities/pair';
import { Token } from '@uniswap/sdk-core';
import Web3 from 'web3';
import { QueryTypes } from 'sequelize/dist';
import { AppService } from './app.service';
import CACHE from 'src/config/cache';
import { TaskNetworkInfo, TaskStakingAPR } from './model/task.model';

const PAIR_ABI: any[] = [
  {
    constant: true,
    inputs: [],
    name: 'getReserves',
    outputs: [
      {
        internalType: 'uint112',
        name: 'reserve0',
        type: 'uint112',
      },
      {
        internalType: 'uint112',
        name: 'reserve1',
        type: 'uint112',
      },
      {
        internalType: 'uint32',
        name: 'blockTimestampLast',
        type: 'uint32',
      },
    ],
    payable: false,
    stateMutability: 'view',
    type: 'function',
  },
];

const SMART_CHEF_ABI: any[] = [
  {
    inputs: [],
    name: 'rewardPerBlock',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
];

const TOKEN_ABI: any[] = [
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'balanceOf',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'totalSupply',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
];

const BN1e18 = '1000000000000000000';

@Injectable()
export class TaskService {
  constructor(private readonly redisService: RedisService) {}

  @Cron('*/5 * * * * *')
  async fetchTokenPrice() {
    console.log('start fetchTokenPrice task!');
    const dataString = await this.redisService.retrieveData(
      CACHE.TASK_RESERVE_TOKEN,
      async () => {
        const queryResult =
          await AppService.getSequelizeConnection().query<TaskNetworkInfo>(
            `
              SELECT n.chain_id, n.network_rpc, n.pair_code_hash, n.factory_contract, (
                SELECT JSON_ARRAYAGG(
                  JSON_OBJECT(
                    'id', t.id,
                    'contract_address', t.contract_address,
                    'token_decimal', t.token_decimal,
                    'token_symbol', t.token_symbol,
                    'route', (
                      SELECT JSON_ARRAYAGG(
                        JSON_OBJECT(
                          'source_token', trr.source_token,
                          'source_token_decimal', trr.source_token_decimal,
                          'target_token', trr.target_token,
                          'target_token_decimal', trr.target_token_decimal
                        )
                      )
                      FROM token_reserve_route trr
                      WHERE trr.token_id = t.id
                      ORDER BY trr.position ASC
                    )
                  )
                )
                FROM token t
                WHERE t.chain_id = n.chain_id
              ) token_info
              FROM network n
            `,
            { type: QueryTypes.SELECT },
          );
        return JSON.stringify(queryResult);
      },
    );
    const taskNetworkInfo: TaskNetworkInfo[] = JSON.parse(dataString);
    for (let i = 0; i < taskNetworkInfo.length; i++) {
      const networkInfo = taskNetworkInfo[i];
      const web3 = new Web3(networkInfo.network_rpc);
      const toBN = web3.utils.toBN;
      for (let j = 0; j < networkInfo.token_info.length; j++) {
        const token = networkInfo.token_info[j];
        try {
          let ratio = toBN(1e18);
          for (let i = 0; i < token.route.length; i++) {
            const route = token.route[i];
            const tokenA = new Token(
              networkInfo.chain_id,
              route.source_token,
              route.source_token_decimal,
            );

            const tokenB = new Token(
              networkInfo.chain_id,
              route.target_token,
              route.target_token_decimal,
            );
            const address = Pair.getAddress(
              networkInfo.pair_code_hash,
              networkInfo.factory_contract,
              tokenA,
              tokenB,
            );
            const pairContract = new web3.eth.Contract(PAIR_ABI, address);
            const reserveResult = await pairContract.methods
              .getReserves()
              .call();
            const balances = tokenA.sortsBefore(tokenB)
              ? [reserveResult[0], reserveResult[1]]
              : [reserveResult[1], reserveResult[0]];
            ratio = ratio.mul(toBN(balances[1])).div(toBN(balances[0]));
          }
          if (!AppService.tokenPrice[networkInfo.chain_id]) {
            AppService.tokenPrice[networkInfo.chain_id] = {};
          }
          AppService.tokenPrice[networkInfo.chain_id][token.contract_address] =
            ratio.div(toBN(1e14)).toNumber() / 1e4;
        } catch (e) {
          console.log('Error: ', e.message);
        }
      }
    }
    console.log('AppService.tokenPrice', AppService.tokenPrice);
    return '0';
  }

  @Cron('*/10 * * * * *')
  async fetchStakeAPR() {
    console.log('start fetchAPR task!');
    const dataString = await this.redisService.retrieveData(
      CACHE.TASK_STAKING_APR,
      async () => {
        const queryResult =
          await AppService.getSequelizeConnection().query<TaskStakingAPR>(
            `
              SELECT n.chain_id, n.network_rpc, n.block_time, (
                SELECT JSON_ARRAYAGG(
                  JSON_OBJECT(
                    'id', si.id,
                    'smart_chef_contract', si.smart_chef_contract,
                    'stake_token', si.stake_token,
                    'reward_token', si.reward_token,
                    'is_lp_staking', si.is_lp_staking,
                    'token', si.token,
                    'quote_token', si.quote_token
                  )
                )
                FROM staking_item si
                WHERE si.chain_id = n.chain_id
              ) staking_info
              FROM network n
            `,
            { type: QueryTypes.SELECT },
          );
        return JSON.stringify(queryResult);
      },
    );
    const taskStakingAPR: TaskStakingAPR[] = JSON.parse(dataString);
    for (let i = 0; i < taskStakingAPR.length; i++) {
      const networkInfo = taskStakingAPR[i];
      const web3 = new Web3(networkInfo.network_rpc);
      const blockPerYear = Math.floor(31536000 / networkInfo.block_time);
      const toBN = web3.utils.toBN;
      if (!AppService.tokenPrice[networkInfo.chain_id]) {
        continue;
      }

      for (let j = 0; j < networkInfo.staking_info.length; j++) {
        const stakingItem = networkInfo.staking_info[j];
        const currentToken = stakingItem.is_lp_staking
          ? stakingItem.token
          : stakingItem.stake_token;
        if (
          !currentToken ||
          !AppService.tokenPrice[networkInfo.chain_id][currentToken] ||
          !AppService.tokenPrice[networkInfo.chain_id][stakingItem.reward_token]
        ) {
          continue;
        }
        const smartChefContract = new web3.eth.Contract(
          SMART_CHEF_ABI,
          stakingItem.smart_chef_contract,
        );
        const rewardPerBlock = toBN(
          await smartChefContract.methods.rewardPerBlock().call(),
        );
        const rewardTokenPerYear = rewardPerBlock
          .muln(blockPerYear)
          .div(toBN(BN1e18))
          .toNumber();
        const rewardPerYear =
          rewardTokenPerYear *
          AppService.tokenPrice[networkInfo.chain_id][stakingItem.reward_token];
        let apr = 0;
        if (stakingItem.is_lp_staking && stakingItem.token) {
          const lpTokenContract = new web3.eth.Contract(
            TOKEN_ABI,
            stakingItem.stake_token,
          );
          const totalSupply = toBN(
            await lpTokenContract.methods.totalSupply().call(),
          )
            .div(toBN(BN1e18))
            .toNumber();
          const totalStake = toBN(
            await lpTokenContract.methods
              .balanceOf(stakingItem.smart_chef_contract)
              .call(),
          )
            .div(toBN(BN1e18))
            .toNumber();
          const tokenContract = new web3.eth.Contract(
            TOKEN_ABI,
            stakingItem.token,
          );
          const totalStakeToken = toBN(
            await tokenContract.methods
              .balanceOf(stakingItem.stake_token)
              .call(),
          )
            .muln(2)
            .div(toBN(BN1e18))
            .toNumber();
          const totalStakeValue =
            (totalStake / totalSupply) *
            totalStakeToken *
            AppService.tokenPrice[networkInfo.chain_id][currentToken];
          apr = (rewardPerYear / totalStakeValue) * 100;
          if (!AppService.stakingAPR[networkInfo.chain_id]) {
            AppService.stakingAPR[networkInfo.chain_id] = {};
          }
          AppService.stakingAPR[networkInfo.chain_id][
            stakingItem.smart_chef_contract
          ] = apr;
        } else {
          const tokenContract = new web3.eth.Contract(
            TOKEN_ABI,
            stakingItem.stake_token,
          );
          const totalStakeToken = toBN(
            await tokenContract.methods
              .balanceOf(stakingItem.smart_chef_contract)
              .call(),
          )
            .div(toBN(BN1e18))
            .toNumber();
          const totalStakeValue =
            totalStakeToken *
            AppService.tokenPrice[networkInfo.chain_id][currentToken];
          apr = (rewardPerYear / totalStakeValue) * 100;
        }

        if (!AppService.stakingAPR[networkInfo.chain_id]) {
          AppService.stakingAPR[networkInfo.chain_id] = {};
        }
        AppService.stakingAPR[networkInfo.chain_id][
          stakingItem.smart_chef_contract
        ] = apr;
      }
    }

    console.log('AppService.tokenAPR', AppService.stakingAPR);
    return '0';
  }
}
