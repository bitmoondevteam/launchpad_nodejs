import { Module } from '@nestjs/common';
import { ProjectService } from './project.service';
import { ProjectController } from './project.controller';
import { RedisService } from 'src/handler/redis.service';
import { HelperService } from 'src/handler/helper.service';

@Module({
  imports: [],
  controllers: [ProjectController],
  providers: [ProjectService, RedisService, HelperService],
})
export class ProjectModule {}
