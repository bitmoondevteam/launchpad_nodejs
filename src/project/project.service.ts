import { BadRequestException, Injectable } from '@nestjs/common';
import { QueryTypes } from 'sequelize/dist';
import { AppService } from 'src/app.service';
import { Project } from 'src/model/project.model';
import CACHE from 'src/config/cache';
import { RedisService } from 'src/handler/redis.service';
import { HelperService } from 'src/handler/helper.service';
import { ProjectResponseDTO } from 'src/dto/project_response.dto';

@Injectable()
export class ProjectService {
  constructor(
    private readonly redisService: RedisService,
    private readonly helperService: HelperService,
  ) {}

  async getListProject(
    page: number,
    limit: number,
  ): Promise<ProjectResponseDTO> {
    const dataString = await this.redisService.retrieveData(
      this.helperService.formatString(CACHE.PROJECT_LIST, page, limit),
      async () => {
        let queryString;
        if (limit != 0) {
          queryString = `SELECT * FROM project LIMIT ${limit} OFFSET ${
            page * limit
          }`;
        } else {
          queryString = 'SELECT * FROM project';
        }
        const projects =
          await AppService.getSequelizeConnection().query<Project>(
            queryString,
            { type: QueryTypes.SELECT },
          );
        return JSON.stringify(projects);
      },
    );
    const totalDataString = await this.redisService.retrieveData(
      CACHE.PROJECT_TOTAL,
      async () => {
        const total: any = await AppService.getSequelizeConnection().query(
          `SELECT COUNT(*) as count FROM project`,
          { type: QueryTypes.SELECT },
        );
        return total[0].count.toString();
      },
    );
    const response: ProjectResponseDTO = {
      data: JSON.parse(dataString),
      metadata: {
        page: page,
        limit: limit,
        total: parseInt(totalDataString),
      },
    };
    return response;
  }

  async getProjectDetail(projectId: number): Promise<Project> {
    const dataString = await this.redisService.retrieveData(
      this.helperService.formatString(CACHE.PROJECT_DETAIL, projectId),
      async () => {
        const project =
          await AppService.getSequelizeConnection().query<Project>(
            `SELECT * FROM project WHERE id = :id`,
            {
              replacements: { id: projectId },
              type: QueryTypes.SELECT,
            },
          );
        return JSON.stringify(project[0]);
      },
    );
    if (!dataString) {
      throw new BadRequestException('Invalid project id');
    }
    return JSON.parse(dataString);
  }
}
