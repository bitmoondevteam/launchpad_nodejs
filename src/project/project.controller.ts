import { BadRequestException, Controller, Get, Query } from '@nestjs/common';
import { ProjectService } from './project.service';

@Controller('project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Get('/list')
  async getListStaking(
    @Query('page') page?: number,
    @Query('limit') limit?: number,
  ) {
    return this.projectService.getListProject(page || 0, limit || 0);
  }

  @Get('/detail')
  async getProjectDetail(@Query('projectId') projectId?: number) {
    if (!projectId) {
      throw new BadRequestException('Missing required query');
    }
    return this.projectService.getProjectDetail(projectId);
  }
}
