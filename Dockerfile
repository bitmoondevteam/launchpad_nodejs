FROM node:lts-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

EXPOSE 4000

RUN apk add --no-cache git  libc6-compat gcompat

# Copy src to workspace
COPY . /usr/src/app 

#Install npm packages
RUN yarn install 

# Build project
RUN yarn build
 
# Use env var NODE_ENV to decide which env will be started
CMD [ "yarn", "start:prod" ]
